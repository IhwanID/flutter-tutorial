# fluttertutorial

Library yang digunakan
----------------------
* [carousel_slider](https://pub.dev/packages/carousel_slider) - untuk menampilkan slide gambar
* [shared_preferences](https://pub.dev/packages/shared_preferences) - untuk menyimpan data lokal
* [fluttertoast](https://pub.dev/packages/fluttertoast) - untuk menampilkan pesan singkat