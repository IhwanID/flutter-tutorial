import 'package:flutter/material.dart';
import 'package:fluttertutorial/ui/detail_screen.dart';
import 'package:fluttertutorial/ui/home_screen.dart';
import 'package:fluttertutorial/ui/login_screen.dart';
import 'package:fluttertutorial/ui/news_screen.dart';
import 'package:fluttertutorial/ui/profile_screen.dart';
import 'package:fluttertutorial/ui/splash_screen.dart';
import 'package:fluttertutorial/utils/storage.dart';

void main() => runApp(MyApp());

/**
 * Material App berisi ruoutes untuk memudahkan navigasi
 */
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: 'splash',
      routes: {
        'splash': (_) => SplashScreen(),
        'main': (_) => DashboardScreen(),
        'login': (_) => LoginScreen(),
        'detail': (_) => DetailScreen(),
      },
    );
  }
}

/**
 * DashboardScreen adalah kode dasar dari ketiga screen yang muncul
 * berisi bottom navigation dan layar yang dynamic tergantung dari button yang aktif
 */
class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  int _selectedIndex = 0;

  setBody() {
    if (_selectedIndex == 0) {
      return HomeScreen();
    } else if (_selectedIndex == 1) {
      return NewsScreen();
    } else {
      return ProfileScreen();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("belajar"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.exit_to_app,
              color: Colors.white,
            ),
            onPressed: () {
              /**
               * ketika tombol logout di appBar ditekan maka akan di arahkan ke halanan login
               * ini adalah cara navigasi yang simple
               */
              Storage().setUserName('');
              Navigator.pushReplacementNamed(context, 'login');
            },
          )
        ],
      ),
      body: setBody(),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Beranda'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.library_books),
            title: Text('Berita'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Profil'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blue,
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
          });
        },
      ),
    );
  }
}
