import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fluttertutorial/utils/storage.dart';

/**
 * Halaman Login
 * Berisi enum untuk pindah kondisi antara login / resgister
 */
class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

// Used for controlling whether the user is login or creating an account
enum FormType { login, register }

class _LoginScreenState extends State<LoginScreen> {
  //variable untuk mengontrol input text
  final TextEditingController _emailFilter = TextEditingController();
  final TextEditingController _passwordFilter = TextEditingController();
  String _email = "";
  String _password = "";
  FormType _form = FormType
      .login; // our default setting is to login, and we should switch to creating an account when the user chooses to

  _LoginScreenState() {
    //menambahkan listener ke variable controller
    _emailFilter.addListener(_emailListen);
    _passwordFilter.addListener(_passwordListen);
  }

  void _emailListen() {
    if (_emailFilter.text.isEmpty) {
      _email = "";
    } else {
      _email = _emailFilter.text;
    }
  }

  void _passwordListen() {
    if (_passwordFilter.text.isEmpty) {
      _password = "";
    } else {
      _password = _passwordFilter.text;
    }
  }

  // mengganti kondisi register / login
  void _formChange() async {
    setState(() {
      if (_form == FormType.register) {
        _form = FormType.login;
      } else {
        _form = FormType.register;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${_form == FormType.login ? "Login" : "Register"}"),
      ),
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 50,
              ),
              FlutterLogo(
                size: 80,
              ),
              SizedBox(
                height: 50,
              ),
              Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      child: TextField(
                        controller: _emailFilter,
                        decoration:
                            InputDecoration(labelText: 'Username / Email'),
                      ),
                    ),
                    Container(
                      child: TextField(
                        controller: _passwordFilter,
                        decoration: InputDecoration(labelText: 'Password'),
                        obscureText: true,
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              _buildButtons(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildButtons() {
    if (_form == FormType.login) {
      return Container(
        child: Column(
          children: <Widget>[
            RaisedButton(
              child: Text('Login'),
              onPressed: _loginPressed,
            ),
            FlatButton(
              child: Text('Dont have an account? Tap here to register.'),
              onPressed: _formChange,
            ),
            FlatButton(
              child: Text('Forgot Password?'),
              onPressed: _passwordReset,
            )
          ],
        ),
      );
    } else {
      return Container(
        child: Column(
          children: <Widget>[
            RaisedButton(
              child: Text('Create an Account'),
              onPressed: _createAccountPressed,
            ),
            FlatButton(
              child: Text('Have an account? Click here to login.'),
              onPressed: _formChange,
            )
          ],
        ),
      );
    }
  }

  //ketika tombol login ditekan, maka akan mengecheck apakah value email & password adalah admin
  //jika benar maka bisa lanjut ke halaman Dashboard, jika gagal muncul pesan singkat untuk register
  void _loginPressed() {
    if (_email == "admin" && _password == "admin") {
      Navigator.pushReplacementNamed(context, 'main');
      Fluttertoast.showToast(msg: "login with $_email and $_password");
    } else {
      Fluttertoast.showToast(msg: "Login Failed, Register First!");
      _formChange();
    }
  }

  //ketika create akun ditekan maka akan menyimpan value username ke lokal storage
  //selama email & password tidak kosong. jika kosong maka akan tampil peringatan singkat
  void _createAccountPressed() {
    if (_email.length > 0 && _password.length > 0) {
      Storage().setUserName(_email);
      Navigator.pushReplacementNamed(context, 'main');
      Fluttertoast.showToast(
          msg: "create an accoutn with $_email and $_password");
    } else {
      Fluttertoast.showToast(msg: "input username & password!");
    }
  }

  //aksi ketika tombol reset ditekan
  void _passwordReset() {
    Fluttertoast.showToast(msg: "Reset password for $_email");
  }
}
