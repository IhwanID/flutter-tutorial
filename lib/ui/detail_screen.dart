import 'package:flutter/material.dart';

/**
 * Halaman Detail
 * Saat ini hanya menampilkan tulisan Detail Screen
 */
class DetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail"),
      ),
      body: Center(child: Text("Detail Screen")),
    );
  }
}
