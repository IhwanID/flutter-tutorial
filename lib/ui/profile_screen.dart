import 'package:flutter/material.dart';

/**
 * Halaman Profil
 * Saat ini hanya menampilkan tulisan Profile Screen
 */
class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text("Profile Screen"),
      ),
    );
  }
}
