import 'package:flutter/material.dart';
import 'package:fluttertutorial/utils/storage.dart';

/**
 * Halaman splash screen.
 * menampilkan logo flutter dan mengecheck apakah user sudah login atau belum
 */
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    checkSession();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FlutterLogo(
          size: 100,
        ),
      ),
    );
  }

  //checking user is login or not by knowing username length in local storage
  void checkSession() async {
    var data = await Storage().getUserName();
    print(data);
    Future.delayed(Duration(milliseconds: 1300), () {
      if (data.length > 0) {
        Navigator.pushReplacementNamed(context, 'main');
      } else {
        Navigator.pushReplacementNamed(context, 'login');
      }
    });
  }
}
