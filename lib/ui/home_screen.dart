import 'dart:math';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  //list url image dari unsplash
  Random _random = new Random();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          CarouselSlider(
            height: 200.0,
            enableInfiniteScroll: true,
            autoPlay: true,
            autoPlayInterval: Duration(seconds: 3),
            items: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((image) {
              return Builder(
                builder: (BuildContext context) {
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Image.asset(
                        'assets/images/$image.jpg',
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                },
              );
            }).toList(),
          ),
          GridView.count(
            physics: ClampingScrollPhysics(),
            shrinkWrap: true,
            crossAxisCount: 2,
            childAspectRatio: 5 / 4,
            children: buildGrid(),
          )
        ],
      ),
    );
  }

  //method untuk membuat grid
  buildGrid() {
    List<Widget> listWidget = [];

    //perulangan sebanyak 30
    for (var index = 0; index < 30; index++) {
      listWidget.add(
        GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, 'detail');
          },
          child: Container(
            margin: EdgeInsets.only(
                left: index.isEven ? 10 : 5,
                right: index.isEven ? 5 : 10,
                top: 5,
                bottom: 10),
            decoration: BoxDecoration(
                color: Colors.blue[400],
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            child: Column(
              children: <Widget>[
                Padding(
                    padding:
                        EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 5),
                    child: Image.asset(
                        "assets/images/${_random.nextInt(9) + 1}.jpg")),
                Text(
                  "Gambar ke-${index + 1}",
                  style: TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                      fontWeight: FontWeight.w800),
                ),
              ],
            ),
          ),
        ),
      );
    }

    return listWidget;
  }
}
