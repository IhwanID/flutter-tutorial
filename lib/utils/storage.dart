import 'package:shared_preferences/shared_preferences.dart';

/**
 * kelas ini digunakan untuk menyimoan data username / email sementara
 * saat splash screen muncul, maka akan mengecheck value dari si [getUserName]
 */

class Storage {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  Future<String> getUserName() async {
    final SharedPreferences prefs = await _prefs;

    return prefs.getString("username") ?? '';
  }

  Future<bool> setUserName(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString("username", value);
  }
}
